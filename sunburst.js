// Dimensions of sunburst.
var width = 1200;
var height = 600;
var radius = Math.min(width, height) / 2;

// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
var b = {
    w: 250, h: 30, s: 3, t: 10
};

// Mapping of step names to colors.
var colors = {
    "Commerce & Communication": "#ce1836",
        "Communications": "#FF4B69",
        "Financial": "#CE1836",
        "Legal": "#E83250",
        "Management": "#A80010",
        "Marketing": "#FF6583",
            "communication": "#FF4B69",
            "journalism": "#F43E5C",
            "accounting": "#DB2543",
            "accounting information systems": "#DB2543",
            "accounting technology": "#DB2543",
            "economics": "#E83250",
            "finance": "#E83250",
            "master of accountancy": "#ce1836",
            "environmental studies & law": "#F43E5C",
            "law": "#DB2543",
            "paralegal studies": "#F43E5C",
            "prelaw": "#E83250",
            "administrative management": "#ce1836",
            "business": "#8E0000",
            "business administration": "#B5001D",
            "entertainment management": "#C10B29",
            "entrepreneurship & small business": "#C10B29",
            "international business": "#CE1836",
            "management": "#A80010",
            "nonprofit administration": "#ce1836",
            "public administration": "#ce1836",
            "sustainable business strategy": "#ce1836",
            "digital marketing": "#FF6583",
            "marketing": "#FF5876",
            "sales & marketing": "#FF718F",

    "Education": "#edb10f",
        "counselor education": "#FABE1C",
        "education": "#E0A402",
        "educational leadership": "#FFD735",
        "health enhancement": "#FFCB29",
        "integrated arts & education": "#FFE442",
    "Health Sciences": "#136666",
        "athletic training": "#005252",
        "environmental health": "#3C8F8F",
        "gerontology": "#378A8A",
        "global public health": "#3C8F8F",
        "health & human performance": "#004242",
        "health medicine": "#328585",
        "medical professions": "#004747",
        "medical technology": "#277A7A",
        "pharmacy": "#186B6B",
        "physical therapy": "#004D4D",
        "premed": "#227575",
        "public health": "#2D8080",
        "radiologic technology": "#277A7A",
        "registered nursing": "#003D3D",
        "social work": "#045757",
        "speechlanguage pathology": "#1D7070",
        "toxicology": "#3C8F8F",
    "Humanities": "#78000c",
        "Culture Studies": "#850D19",
        "Languages": "#5F0000",
        "classics": "#D15965",
        "african american studies": "#921A26",
        "asian studies": "#850D19",
        "central & southwest asian studies": "#850D19",
        "east asian studies": "#850D19",
        "irish studies": "#921A26",
        "native american studies": "#921A26",
        "south & southeast asian studies": "#850D19",
        "film studies": "#921A26",
        "general studies": "#D15965",
        "historic preservation": "#D15965",
        "history": "#AB333F",
        "interdisciplinary studies": "#D15965",
        "arabic": "#6B0000",
        "english": "#5F0000",
        "french": "#6B0000",
        "german": "#6B0000",
        "japanese": "#6B0000",
        "modern & classical languages & literatures": "#6B0000",
        "russian": "#6B0000",
        "spanish": "#6B0000",
        "liberal studies": "#D15965",
        "military studies": "#C54D59",
        "philosophy": "#AB333F",
        "political science": "#9E2632",
    "Nature & Environment": "#537d00",
        "climate change": "#A0CA4D",
        "ecological restoration": "#79A326",
        "environmental science & natural resource journalism": "#ACD659",
        "environmental studies": "#467000",
        "forestry": "#3A6400",
        "parks tourism & recreation management": "#6D971A",
        "resource conservation": "#86B033",
        "systems ecology": "#B9E366",
        "wilderness studies": "#608A0D",
        "wildland fire sciences & management": "#93BD40",
        "wildlife biology": "#2D5700",
    "Science, Math & Technology": "#0070a8",
        "Technology": "#003068",
        "Vocational": "#0D7DB5",
        "Biology": "#00639B",
        "Mathematics": "#2696CE",
        "Earth Science": "#004A82",
            "astronomy": "#4DBDF5",
            "biochemistry": "#47B7EF",
            "biology": "#005B93",
            "cellular molecular & microbial biology": "#0F72AA",
            "microbiology": "#177AB2",
            "organismal biology & ecology": "#086BA3",
            "business analytics": "#0070a8",
            "chemistry": "#3DADE5",
            "geography": "#176199",
            "geosciences": "#08528A",
            "gis sciences & technologies": "#1F69A1",
            "mountain studies": "#0F5991",
            "materials science": "#52C2FA",
            "mathematics": "#1C8CC4",
            "teaching middle school mathematics": "#2696CE",
            "neuroscience": "#38A8E0",
            "physics": "#42B2EA",
            "big data analytics": "#0A3A72",
            "computer science": "#002B63",
            "health information technology": "#1A4A82",
            "management information systems": "#0F3F77",
            "network information security": "#1F4F87",
            "carpentry": "#2191C9",
            "computer aided design": "#1787BF",
            "diesel equipment technology": "#36A6DE",
            "facility management engineering": "#0373AB",
            "food service management": "#40B0E8",
            "heavy equipment operation": "#40B0E8",
            "recreational power equipment": "#40B0E8",
            "welding technology": "#2C9CD4",
            "information technology": "#14447C",
    "Societal & Behavorial Sciences": "#693c64",
        "anthropology": "#764971",
        "clinical psychology": "#5C2F57",
        "experimental psychology": "#5C2F57",
        "forensic studies": "#83567E",
        "human & family development": "#9C6F97",
        "international development studies": "#B689B1",
        "linguistics": "#A97CA4",
        "psychology": "#50234B",
        "school psychology": "#5C2F57",
        "sociology": "#8F628A",
        "womens gender & sexuality studies": "#C295BD",
    "The Arts": "#d93a12",
        "Culinary": "#d93a12",
        "Digital & Visual": "#C72800",
        "Performing": "#EB4C24",
        "Written": "#d93a12",
        "culinary arts": "#FF7048",
        "art": "B51600",
        "media arts": "#B51600",
        "dance": "#FD5E36",
        "music": "#FD5E36",
        "theatre": "#FD5E36",
        "creative writing": "#FF8159",
};

// Total size of all segments; we set this later, after loading the data.
var totalSize = 0;

var vis = d3.select("#chart").append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .append("svg:g")
    .attr("id", "container")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var partition = d3.layout.partition()
    .size([2 * Math.PI, radius * radius])
    .value(function(d) { return d.size; });

var arc = d3.svg.arc()
    .startAngle(function(d) { return d.x; })
    .endAngle(function(d) { return d.x + d.dx; })
    .innerRadius(function(d) { return Math.sqrt(d.y); })
    .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

// Use d3.text and d3.csv.parseRows so that we do not need to have a header
// row, and can receive the csv as an array of arrays.
d3.text("study-categories2.csv", function(text) {
    var csv = d3.csv.parseRows(text);
    var json = buildHierarchy(csv);
    createVisualization(json);
});

// Main function to draw and set up the visualization, once we have the data.
function createVisualization(json) {

    // Basic setup of page elements.
    initializeBreadcrumbTrail();
    drawLegend();
    d3.select("#togglelegend").on("click", toggleLegend);

    // Bounding circle underneath the sunburst, to make it easier to detect
    // when the mouse leaves the parent g.
    vis.append("svg:circle")
        .attr("r", radius)
        .style("opacity", 0);

    // For efficiency, filter nodes to keep only those large enough to see.
    var nodes = partition.nodes(json)
        .filter(function(d) {
            return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
        });

    var path = vis.data([json]).selectAll("path")
        .data(nodes)
        .enter().append("svg:path")
        .attr("display", function(d) { return d.depth ? null : "none"; })
        .attr("d", arc)
        .attr("fill-rule", "evenodd")
        .style("fill", function(d) { return colors[d.name]; })
        .style("opacity", 1)
        .on("mouseover", mouseover);

    // Add the mouseleave handler to the bounding circle.
    d3.select("#container").on("mouseleave", mouseleave);

    // Get total size of the tree = value of root node from partition.
    totalSize = path.node().__data__.value;
};

// Fade all but the current sequence, and show it in the breadcrumb trail.
function mouseover(d) {

    var percentage = (100 * d.value / totalSize).toPrecision(3);
    var percentageString = percentage + "%";
    if (percentage < 0.1) {
        percentageString = "< 0.1%";
    }

    d3.select("#percentage")
        .text(percentageString);

    d3.select("#explanation")
        .style("visibility", "");

    var sequenceArray = getAncestors(d);
    updateBreadcrumbs(sequenceArray, percentageString);

    // Fade all the segments.
    d3.selectAll("path")
        .style("opacity", 0.3);

    // Then highlight only those that are an ancestor of the current segment.
    vis.selectAll("path")
        .filter(function(node) {
            return (sequenceArray.indexOf(node) >= 0);
        })
        .style("opacity", 1);
}

// Restore everything to full opacity when moving off the visualization.
function mouseleave(d) {

    // Hide the breadcrumb trail
    d3.select("#trail")
        .style("visibility", "hidden");

    // Deactivate all segments during transition.
    d3.selectAll("path").on("mouseover", null);

    // Transition each segment to full opacity and then reactivate it.
    d3.selectAll("path")
        .transition()
        .duration(1000)
        .style("opacity", 1)
        .each("end", function() {
            d3.select(this).on("mouseover", mouseover);
        });

    d3.select("#explanation")
        .style("visibility", "hidden");
}

// Given a node in a partition layout, return an array of all of its ancestor
// nodes, highest first, but excluding the root.
function getAncestors(node) {
    var path = [];
    var current = node;
    while (current.parent) {
        path.unshift(current);
        current = current.parent;
    }
    return path;
}

function initializeBreadcrumbTrail() {
    // Add the svg area.
    var trail = d3.select("#sequence").append("svg:svg")
        .attr("width", width)
        .attr("height", 50)
        .attr("id", "trail");
    // Add the label at the end, for the percentage.
    trail.append("svg:text")
        .attr("id", "endlabel")
        .style("fill", "#000");
}

// Generate a string that describes the points of a breadcrumb polygon.
function breadcrumbPoints(d, i) {
    var points = [];
    points.push("0,0");
    points.push(b.w + ",0");
    points.push(b.w + b.t + "," + (b.h / 2));
    points.push(b.w + "," + b.h);
    points.push("0," + b.h);
    if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
        points.push(b.t + "," + (b.h / 2));
    }
    return points.join(" ");
}

// Update the breadcrumb trail to show the current sequence and percentage.
function updateBreadcrumbs(nodeArray, percentageString) {

    // Data join; key function combines name and depth (= position in sequence).
    var g = d3.select("#trail")
        .selectAll("g")
        .data(nodeArray, function(d) { return d.name + d.depth; });

    // Add breadcrumb and label for entering nodes.
    var entering = g.enter().append("svg:g");

    entering.append("svg:polygon")
        .attr("points", breadcrumbPoints)
        .style("fill", function(d) { return colors[d.name]; });

    entering.append("svg:text")
        .attr("x", (b.w + b.t) / 2)
        .attr("y", b.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .text(function(d) { return d.name; });

    // Set position for entering and updating nodes.
    g.attr("transform", function(d, i) {
        return "translate(" + i * (b.w + b.s) + ", 0)";
    });

    // Remove exiting nodes.
    g.exit().remove();

    // Now move and update the percentage at the end.
    d3.select("#trail").select("#endlabel")
        .attr("x", (nodeArray.length + 0.5) * (b.w + b.s))
        .attr("y", b.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .text(percentageString);

    // Make the breadcrumb trail visible, if it's hidden.
    d3.select("#trail")
        .style("visibility", "");

}

function drawLegend() {

    // Dimensions of legend item: width, height, spacing, radius of rounded rect.
    var li = {
        w: 250, h: 30, s: 3, r: 3
    };

    var legend = d3.select("#legend").append("svg:svg")
        .attr("width", li.w)
        .attr("height", d3.keys(colors).length * (li.h + li.s));

    var g = legend.selectAll("g")
        .data(d3.entries(colors))
        .enter().append("svg:g")
        .attr("transform", function(d, i) {
            return "translate(0," + i * (li.h + li.s) + ")";
        });

    g.append("svg:rect")
        .attr("rx", li.r)
        .attr("ry", li.r)
        .attr("width", li.w)
        .attr("height", li.h)
        .style("fill", function(d) { return d.value; });

    g.append("svg:text")
        .attr("x", li.w / 2)
        .attr("y", li.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .text(function(d) { return d.key; });
}

function toggleLegend() {
    var legend = d3.select("#legend");
    if (legend.style("visibility") == "hidden") {
        legend.style("visibility", "");
    } else {
        legend.style("visibility", "hidden");
    }
}

// Take a 2-column CSV and transform it into a hierarchical structure suitable
// for a partition layout. The first column is a sequence of step names, from
// root to leaf, separated by hyphens. The second column is a count of how 
// often that sequence occurred.
function buildHierarchy(csv) {
    var root = {"name": "root", "children": []};
    for (var i = 0; i < csv.length; i++) {
        var sequence = csv[i][0];
        var size = +csv[i][1];
        if (isNaN(size)) { // e.g. if this is a header row
            continue;
        }
        var parts = sequence.split("-");
        var currentNode = root;
        for (var j = 0; j < parts.length; j++) {
            var children = currentNode["children"];
            var nodeName = parts[j];
            var childNode;
            if (j + 1 < parts.length) {
                // Not yet at the end of the sequence; move down the tree.
                var foundChild = false;
                for (var k = 0; k < children.length; k++) {
                    if (children[k]["name"] == nodeName) {
                        childNode = children[k];
                        foundChild = true;
                        break;
                    }
                }
                // If we don't already have a child node for this branch, create it.
                if (!foundChild) {
                    childNode = {"name": nodeName, "children": []};
                    children.push(childNode);
                }
                currentNode = childNode;
            } else {
                // Reached the end of the sequence; create a leaf node.
                childNode = {"name": nodeName, "size": size};
                children.push(childNode);
            }
        }
    }
    return root;
};